## Acute Mammalian Toxicity (AT) ##
---
Refers to those adverse effects occurring following oral or dermal administration
of a single dose of a substance, or multiple doses given within 24 hours, or an
inhalation exposure of 4 hours (GHS, Chapter 3.1: Acute Toxicity. 2009, United
Nations).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---