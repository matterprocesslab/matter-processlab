## GreenScreen Method ##
---
The GreenScreen method can be a useful process to support environmentally preferable design products for human and environmental endpoints in a range from Very High (vH) to Very Low (vL).  It can apply to cases of single or more complex mixtures or materials.

* [Weblink](http://www.greenscreenchemicals.org/method/full-greenscreen-method)

### Very High
---
##### Environmental Fate
* Persistence (P)
* Bioaccumulation (B)

##### Environmental Health
* Acute Aquatic Toxicity (AA)
* Chronic Aquatic Toxicity (CA)

##### Human Health Group I
* Carcinogenicity (C)
* Mutagenicity & Genotoxicity (M)
* Reproductive Toxicity (R)
* Developmental Toxicity (incl. Developmental Neurotoxicity) (D)
* Endocrine Activity (E)

##### Human Health Group II
* Acute Mammalian Toxicity (AT)
* Systemic Toxicity & Organ Effects (incl. Immunotoxicity) (ST)
* Toxicity (R)	
* Neurotoxicity (N)
* Sensitization (SnS)

##### Physical Hazards
* Reactivity (Rx)
* Flammability (F)

---
### Very Low