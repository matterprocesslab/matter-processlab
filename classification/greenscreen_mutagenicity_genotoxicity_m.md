## Mutagenicity & Genotoxicity (M)
---
The more general terms genotoxic and genotoxicity apply to agents or processes
which alter the structure, information content, or segregation of DNA, including
those which cause DNA damage by interfering with normal replication (from http://
www.epa.gov/sites/production/files/2014-01/documents/aa_criteria_v2.pdf).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---