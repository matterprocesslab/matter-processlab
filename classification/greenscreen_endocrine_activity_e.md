## Endocrine Activity (E)
---
An endocrine active substance is a substance having the inherent ability to
interact or interfere with one or more components of the endocrine system
resulting in a biological effect, but need not necessarily cause adverse effects.
Endocrine activity is considered as a collection of modes of action, potentially
leading to adverse outcomes, rather than a (eco)toxicological hazard in itself
(http://www.efsa.europa.eu/en/efsajournal/pub/3132.htm).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---