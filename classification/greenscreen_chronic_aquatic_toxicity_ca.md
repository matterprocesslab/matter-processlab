## Chronic Aquatic Toxicity (CA) ##
---
The intrinsic property of a substance to cause adverse effects to aquatic organisms
during aquatic exposures which are determined in relation to the life-cycle
of the organism (GHS, Chapter 4.1: Hazards to the Aquatic Environment. 2009,
United Nations). 

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---