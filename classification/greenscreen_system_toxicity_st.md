## Systemic Toxicity (ST)
---
Includes all significant non-lethal effects in a single organ that can impair function,
both reversible and irreversible, immediate and/or delayed, not otherwise covered
by any other endpoint; or generalized changes of a less severe nature involving
several organs. 

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---