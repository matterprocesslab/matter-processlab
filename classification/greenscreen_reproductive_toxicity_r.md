## Reproductive Toxicity (R)
---
The occurrence of biologically adverse effects on the reproductive systems of
females or males that may result from exposure to environmental agents. The
toxicity may be expressed as alterations to the female or male reproductive
organs, the related endocrine system, or pregnancy outcomes. The manifestation
of such toxicity may include, but not be limited to, adverse effects on onset of
puberty, gamete production and transport, reproductive cycle normality, sexual
behavior, fertility, gestation, parturition, lactation, developmental toxicity, premature
reproductive senescence, or modifications in other functions that are
dependent on the integrity of the reproductive systems (USEPA, Guidelines
for Reproductive Toxicity Risk Assessment. Federal Register, 1996. 61(212):
p. 56274-56322).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---