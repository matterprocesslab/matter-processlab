## Skin Irritation (IrS)
---
The production of reversible damage to the skin following the application of a test
substance for up to 4 hours (GHS, Chapter 3.2: Skin Corrosion/Irritation. 2009,
United Nations).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---