## Respiratory Sensitization (SnR)
---
Hypersensitivity of the airways following inhalation of the substance
(GHS, Chapter 3.4: Respiratory or Skin Sensitization. 2009, United Nations).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---