## Developmental Toxicity (D)
---
Adverse effects in the developing organism that may result from exposure prior
to conception (either parent), during prenatal development, or postnatally to the
time of sexual maturation. Adverse developmental effects may be detected at any
point in the lifespan of the organism. The major manifestations of developmental
toxicity include: (1) death of the developing organism, (2) structural abnormality,
(3) altered growth, and (4) functional deficiency (USEPA, Guidelines for
Developmental Toxicity Risk Assessment. Federal Register, 1991. 56(234):
p. 63798-63826).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---