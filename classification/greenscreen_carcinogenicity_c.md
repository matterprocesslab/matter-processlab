## Carcinogenicity (C)
---
Capable of increasing the incidence of malignant neoplasms, reducing their
latency, or increasing their severity or multiplicity (IARC. Preamble to the IARC
Monographs: A. General Principles And Procedures: 2. Objective and scope. 2006
[cited 2011 June 20]; Available from: http://monographs.iarc.fr/ENG/Preamble/
currenta2objective0706.php).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---