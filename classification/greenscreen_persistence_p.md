## Persistence (P)
---
The length of time the chemical can exist in the environment before being
destroyed (i.e., transformed) by natural processes (http://www.who.int/ceh/
publications/endocrine/en/index.html). 

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---