## Acute Aquatic Toxicity (AA) ##
---
The intrinsic property of a substance to be injurious to an organism in a
short-term, aquatic exposure to that substance (GHS, Chapter 4.1: Hazards
to the Aquatic Environment. 2009, United Nations).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---