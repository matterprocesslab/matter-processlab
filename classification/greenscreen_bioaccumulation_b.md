## Bioaccumulation (B) ##
---
A process in which a chemical substance is absorbed in an organism by all
routes of exposure as occurs in the natural environment (e.g., dietary and ambient
environment sources). Bioaccumulation is the net result of competing processes
of chemical uptake into the organism at the respiratory surface and from the
diet and chemical elimination from the organism including respiratory exchange,
fecal egestion, metabolic biotransformation of the parent compound and growth
dilution (Arnot, J.A. and F.A. Gobas, A review of bioconcentration factor (BCF)
and bioaccumulation factor (BAF) assessments for organic chemicals in aquatic
organisms. Environmental Reviews, 2006. 14: p. 257-297).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

---