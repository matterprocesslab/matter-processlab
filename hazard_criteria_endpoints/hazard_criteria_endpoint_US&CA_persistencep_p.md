## Persistence (P)
---
The length of time the chemical can exist in the environment before being
destroyed (i.e., transformed) by natural processes (http://www.who.int/ceh/
publications/endocrine/en/index.html). 

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/3_GreenScreen_Hazard_Criteria_v13_2016_3_8.pdf)

|  Reference |  Very High | High  | Moderate  |  Low |
|---|---|---|---|---|
| Soil or Sediment (1/2 life in days OR Result) |  >180 or recalcitrant | >60-180  |  16-60  |  <16 OR GHS “Rapid degradability”  |
| Water (1/2 life in days OR Result) | >60 or recalcitrant | >40-60  | 16-40  | <16 OR GHS “Rapid degradability” |
| Air (1/2 life in days OR Result) | >5 or recalcitrant  | >2-5 | ---  |  <2 |
| Long-Range Environmental Transport  |  ≤2 |  >2-10 |  >10-20 | >20  |
| EC – CEPA DSL | ≤0.5 | >0.5–1.0 | >1–5 | >5 |

---

* EPCRA Extremely Hazardous Substances, Authoritative Type
