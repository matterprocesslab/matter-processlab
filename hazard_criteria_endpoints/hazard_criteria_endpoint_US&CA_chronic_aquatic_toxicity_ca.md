## Chronic Aquatic Toxicity (CA) ##
---
The intrinsic property of a substance to cause adverse effects to aquatic organisms
during aquatic exposures which are determined in relation to the life-cycle
of the organism (GHS, Chapter 4.1: Hazards to the Aquatic Environment. 2009,
United Nations). 

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

|  Reference |  Very High | High  | Moderate  |  Low | Very Low |
|---|---|---|---|---|---|
| GHS Criteria & Guidance | --- | --- | GHS Category 4 | >10 | --- |
| Guidance Value (mg/L) | ≤0.1 | >0.1 to 1.0 | > 1.0 to 10  | >10 | --- |
| EC – CEPA DSL | non-human - acute or chronic aquatic toxicity. | --- | --- | --- |--- |