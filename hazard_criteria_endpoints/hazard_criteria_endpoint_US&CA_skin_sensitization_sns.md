## Skin Sensitization (SnS) ##
---
A skin sensitizer is a substance that will lead to an allergic response following skin contact (http://www.unece.org/fileadmin/DAM/trans/danger/publi/ghs/ ghs_rev04/English/03e_part3.pdf).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

|  Reference |  Very High | High  | Moderate  |  Low |
|---|---|---|---|---|
| GHS Criteria & Guidance | --- | 1A (High Frequency of Occurrence) |  1B (Low to Moderate Frequency of Occurrence) | --- |
| MAK | --- | Airway and Skin | --- | --- |