## Bioaccumulation (B) ##
---
A process in which a chemical substance is absorbed in an organism by all
routes of exposure as occurs in the natural environment (e.g., dietary and ambient
environment sources).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

|  Reference |  Very High | High  | Moderate  |  Low | Very Low |
|---|---|---|---|---|---|
| Bioaccumulation Factor BAF  | >5000 | >1000-5000 | >500-1000 | >100 -500 | ≤ 100 |
| Bioconcentration Factor BCF | >5000 | >1000-5000 | >500-1000  | >100-500 | ≤ 100 |
| Log octanol-water partition coefficient Log Kow | >5.0 | >4.5-5.0 | >4.0-4.5 | --- | ≤4 |
| Presence in humans or wildlife | --- | Evidence | Suggestive Evidence | --- |--- |
| EC – CEPA DSL | Bioaccumulative | --- | --- | --- |--- |