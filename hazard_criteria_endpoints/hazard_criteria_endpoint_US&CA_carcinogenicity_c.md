## Carcinogenicity (C)
---
Capable of increasing the incidence of malignant neoplasms, reducing their latency, or increasing their severity or multiplicity (IARC. Preamble to the IARC Monographs: A. General Principles And Procedures: 2. Objective and scope. 2006 [cited 2011 June 20]; Available from: http://monographs.iarc.fr/ENG/Preamble/ currenta2objective0706.php). 

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/3_GreenScreen_Hazard_Criteria_v13_2016_3_8.pdf)

|  Reference |  High  | Moderate  |  Low |
|---|---|---|---|
| GHS Criteria  |  1A or 1B |  2  | --- |
|US EPA – IRIS Carcinogens (1986) |Known or Likely|---|Not Likely|
|US EPA – IRIS Carcinogens
(1996, 1999, 2005)|Category 1 or 2|Category 3|---|
|IARC|Group 1 or 2a|Group 2b|Group 4|
|MAK|Carcinogen Group 1 or 2|Carcinogen Group 3A or 3B, 4, or 5|---|
|US CDC – Occupational Carcinogens|Occupational Carcinogen|---|---|
|US NIH – Report on Carcinogens|Known or Reasonably Anticipated|---|---|
|CA EPA -- Prop 65|Carcinogen|---|---|
|US EPA – IRIS Carcinogens (1986)|Group D|
|US EPA – IRIS Carcinogens (1999)|Suggestive Evidence, but not sufficient to assess human carcinogenic potential|
|US EPA – IRIS Carcinogens (2005)|Suggestive evidence of carcinogenic potential|
|IARC|Group 3|
|CA EPA – Prop 65 (with qualifications)*|Carcinogen – specific to chemical form or exposure route|

* Hazards may be form-specific or based on limited exposure pathways. Listing of a chemical should always be supported by data from literature.
