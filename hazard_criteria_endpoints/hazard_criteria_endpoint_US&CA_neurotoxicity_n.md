## Neurotoxicity (N) ##
---
An adverse change in the structure or function of the central and/or peripheral nervous system following exposure to a chemical, or a physical or biological agent (USEPA, Guidelines for Neurotoxicity Risk Assessment. Federal Register, 1998. 63(93): p. 26926-26954).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

|  Reference |  Very High | High  | Moderate  |  Low |
|---|---|---|---|---|
| GHS Criteria & Guidance | --- | 1A (High Frequency of Occurrence) |  1B (Low to Moderate Frequency of Occurrence) | --- |
| MAK | --- | Airway and Skin | --- | --- |