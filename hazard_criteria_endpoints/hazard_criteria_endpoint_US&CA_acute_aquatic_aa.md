## Acute Aquatic Toxicity (AA) ##
---
The intrinsic property of a substance to be injurious to an organism in a
short-term, aquatic exposure to that substance (GHS, Chapter 4.1: Hazards
to the Aquatic Environment. 2009, United Nations).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/3_GreenScreen_Hazard_Criteria_v13_2016_3_8.pdf)

|  Reference |  Very High | High  | Moderate  |  Low |
|---|---|---|---|---|
| GHS Criteria  | 1 | 2 | 3 |  --  |
| LD50 or EC50 mg/L | ≤1 | >1-10 | >10-100  |  >100 |
| EC – CEPA DSL | Non-human acute or chronic |