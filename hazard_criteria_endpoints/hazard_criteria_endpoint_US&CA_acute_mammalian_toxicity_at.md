## Acute Mammalian Toxicity (AT)
---
Includes all significant non-lethal effects in a single organ that can impair function, both reversible and irreversible, immediate and/or delayed, not otherwise covered by any other endpoint; or generalized changes of a less severe nature involving several organs. 

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/3_GreenScreen_Hazard_Criteria_v13_2016_3_8.pdf)

|  Reference |  Very High | High  | Moderate  |  Low |
|---|---|---|---|---|
| GHS Criteria  |  1 or 2 |  3  |  4  |  5  |
| Oral LD50 mg/kg |  ≤50 | >50-300  | >300-2000  |  >2000 |
| Dermal LD50 mg/kg | ≤200  | >200-1000  | >1000-2000  |  >2000 |
| Inhalation Gas/Vapor LC50 mg/L |  ≤2 |  >2-10 |  >10-20 | >20  |
| Inhalation Dust/Mist/Fumes Lc50 mg/L | ≤0.5 | >0.5–1.0 | >1–5 | >5 |
| B List US EPA-EPCRA | * |  * |   |   |

* EPCRA Extremely Hazardous Substances, Authoritative Type
