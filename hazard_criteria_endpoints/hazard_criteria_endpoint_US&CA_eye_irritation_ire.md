## Eye Irritation (IrE) ##
---
Eye irritation is the production of changes in the eye following the application of a test substance to the anterior surface of the eye, which are fully reversible within 21 days of application (http://www.unece.org/fileadmin/DAM/trans/ danger/publi/ghs/ghs_rev04/English/03e_part3.pdf).

* [Weblink](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)

|  Reference |  Very High | High  | Moderate  |  Low |
|---|---|---|---|---|
| GHS Criteria & Guidance | 1 (Irreversible) | 2A (Irritating) |  2B (Mildly irritating) | --- |