# Article Research #
---
* GreenScreen Method
    * [Main Weblink](http://www.greenscreenchemicals.org/method/full-greenscreen-method)
    * [GreenScreen Guidance](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/1_GreenScreen_Guidance_v13_2016_3_8.pdf)
    * [GreenScreen Hazard Criteria](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/3_GreenScreen_Hazard_Criteria_v13_2016_3_8.pdf)
    * [GreenScreen Benchmark Criteria](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/GreenScreen_Benchmark_Criteria_v13_2016_3_8.pdf)
    * [GreenScreen Assessment Report Template](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/4_GreenScreen_Assessment_Report_Template_v13_2016_3_8.docx)
    * [GreenScreen Information Services](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/5_GreenScreen_Information_Sources_v13_2016_3_8.pdf)
    * [GreenScreen List Translator](http://www.greenscreenchemicals.org/static/ee_images/uploads/resources/2_GreenScreen_List_Translator_v1_2016_3_8.pdf)
* GHS Classification Hazards and Categories
    * [GHS Classification](http://www.idri.org/GHS/HazClassSummary.pdf)
* NIST Standard Reference Database 81: Heat Transmission Properties of Insulating and Building Materials | National Institute of Standards and Technology
    * [Weblink](http://srdata.nist.gov/insulation/)
* Materials transparency & risk for architects: an introduction to advancing professional ethics while managing professional liability risks
    * [Weblink](http://www.aia.org/aiaucmp/groups/aia/documents/pdf/aiab108448.pdf)
* COTE top ten projects report compres performance, cost, location and trend.
    * [Weblink](http://www.aia.org/aiaucmp/groups/aia/documents/pdf/aiab108457.pdf)
    * [AIA Article](http://www.architectmagazine.com/practice/the-aia-committee-on-the-environment-examines-the-state-of-high-design-high-performance-buildings_o)